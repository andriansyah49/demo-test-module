const process = require('./libs/process');

/**
 * @description Aggregated Main Class
 * @method api
 */
 class Aggregate {
    /**
     * @description Constructor to init aggregate
     * @param service_name - string
     * @param access_key - string
     * @param delay - number
     * @returns 
     */
    constructor(service_name, access_key, delay) {
      this.service_name = service_name;
      this.access_key = access_key;
      this.delay = delay;
    }

    /**
     * @description Post Aggregated API Calls
     * @param api_name - string
     * @param env - string
     * @param instance - string
     * @param version - string
     * @param agg_calls_count - number
     * @param agg_response_time - number
     * @returns 
     */
    api(api_name, env, instance, version, agg_calls_count, agg_response_time) {
        // API path name
        let path = 'api';
        
        // Data to be send to API
        let data = [
            {
                service_name: this.service_name,
                api_name : api_name ? api_name : '',
                env : env ? env : '',
                instance : instance ? instance : '',
                version : version ? version : '',
                agg_calls_count : agg_calls_count ? agg_calls_count : 0,
                agg_response_time : agg_response_time ? agg_response_time : 0
            }
        ];

        const post = process.postAggregate(this.access_key, this.service_name, path, data)      

        return post;
    }

    /**
     * @description Post Aggregated Video Calls
     * @param accountid - string
     * @param videoid - number
     * @param ownerid - string
     * @param rendition - string
     * @param agg_seconds_watched - number
     * @param agg_views - number
     * @returns 
     */
    video(accountid, videoid, ownerid, rendition, agg_seconds_watched, agg_views) {
        // API path name
        let path = 'video';
        
        // Data to be send to API
        let data = [
            {
                service_name: this.service_name,
                accountid : accountid ? accountid : '',
                videoid : videoid ? videoid : '',
                ownerid : ownerid ? ownerid : '',
                rendition : rendition ? rendition : '',
                agg_seconds_watched : agg_seconds_watched ? agg_seconds_watched : 0,
                agg_views : agg_views ? agg_views : 0
            }
        ];

        const post = process.postAggregate(this.access_key, this.service_name, path, data)      

        return post;
    }

    /**
     * @description Post Aggregated Page Views
     * @param accountid - string
     * @param pageid - string
     * @param domain - string
     * @param agg_views - number
     * @returns 
     */
     page(accountid, pageid, domain, agg_views) {
        // API path name
        let path = 'page';
        
        // Data to be send to API
        let data = [
            {
                service_name: this.service_name,
                accountid : accountid ? accountid : '',
                pageid : pageid ? pageid : '',
                domain : domain ? domain : '',
                agg_views : agg_views ? agg_views : 0
            }
        ];

        const post = process.postAggregate(this.access_key, this.service_name, path, data)      

        return post;
    }
}

module.exports = {
    Aggregate
};