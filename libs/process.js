const axios = require('axios');

/**
 * @description Post Data to Jixie Aggreggated API
 * @param access_key - string
 * @param service_name - string
 * @param path - string
 * @param data - array of object
 * @returns 
 */
const postAggregate = async(access_key, service_name, path, data) => {
    let post = axios({
        method: 'post',
        url: `https://jixie-aggregated-analytics-rc.azurewebsites.net/${path}`,
        data: data,
        headers: {
            JIXIE_ACCESS_KEY: access_key,
            JIXIE_SERVICE: service_name
        }
    }).then((response) => {
        if (response.status === 204) {
            return true;
        }else{
            return false;
        }
    }).catch((error) => {
        console.log(error);
        return false;
    });

    return post;
}

module.exports = {
    postAggregate
}